<?xml version="1.0"?>
<!--Copyright (c) HealthLink 2015 All Right Reserved-->
<!--Author Chandan Datta (chandan.datta@healthlink.net)-->
<!--Transformation is done in 2 stages.-->
<!--First stage is creating XSLT from REQUEST-->
<!--Second stage is applying XSLT from first stage to mock_data.xml-->
<!--This file is used to create xslt from request.-->

<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
				xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias">
	<xsl:output method="xml" version="1.0" omit-xml-declaration="no" indent="yes" />
	<xsl:param name="versionParam" select="'1.0'" />
    <!--alias for prefix xsl in result-->
	<xsl:namespace-alias stylesheet-prefix="axsl" result-prefix="xsl"/>

	<xsl:template match="/"> <!-- apply transformation to root -->
		<axsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'> <!-- create head of result file -->
            <axsl:output method="xml" version="1.0" omit-xml-declaration="no" indent="yes" />
            <axsl:param name="versionParam" select="'1.0'" />
            <axsl:template match="/"> <!-- apply transformation to root in result file -->
                <xsl:apply-templates select="@* | node()"/> <!-- apply transformation to all nodes and attributes -->
            </axsl:template>
			<xsl:apply-templates select="//*[@conceptName and (node())]" mode="outside"/>
		</axsl:stylesheet>
	</xsl:template>


    <!--it is a common pattern for all attributes and nodes except particular templates-->
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>  <!-- apply template to each attribute and to each child node -->
		</xsl:copy>
	</xsl:template>

    <!--it is a pattern for nodes with attribute conceptName and child nodes -->
    <xsl:template match="*[@conceptName and (node())]">
		<axsl:choose>
			<axsl:when test="count(//*[@conceptName='{./@conceptName}'])>0"> <!-- if mock_data contains data with this conceptName -->
				<axsl:apply-templates select="//*[@conceptName='{./@conceptName}']"/>  <!-- then apply additional template -->
			</axsl:when>
			<axsl:otherwise>
				<xsl:copy-of select="."/> <!-- else full copy element -->
			</axsl:otherwise>
		</axsl:choose>
    </xsl:template>


    <!--it is a pattern for nodes with attribute conceptName and without child nodes -->
	<xsl:template match="*[@conceptName and not(node())]">
			<xsl:element name="{name(.)}"> <!-- create element with same name -->
				<xsl:apply-templates select="@* | node()"/> <!-- copy all attributes -->
				<axsl:value-of select=".//*[@conceptName='{./@conceptName}']/text()" />  <!-- command for insert value from mock_data -->
			</xsl:element>
	</xsl:template>

    <!--it is a additional pattern for nodes with attribute conceptName and child nodes -->
	<xsl:template match="//*[@conceptName and (node())]" mode="outside">
		<axsl:template match="//*[@conceptName='{./@conceptName}']">
			<xsl:element name="{name(.)}">  <!-- create element with same name -->
				<xsl:apply-templates select="@* | node()"/>  <!-- copy all attributes and apply templates to child nodes -->
			</xsl:element>
		</axsl:template>
	</xsl:template>

</xsl:stylesheet>
