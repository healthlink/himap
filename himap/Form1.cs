﻿// Copyright (c) HealthLink 2015 All Right Reserved
// Author Chandan Datta (chandan.datta@healthlink.net)

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Xsl;
using System.Data.OleDb;
using System.IO;

namespace himap
{
    public partial class MainForm : Form
    {
        System.Text.Encoding enc = System.Text.Encoding.UTF8;

        private String transText2;

        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkXmls()) {
                try
                {
                    Transform1();
                    Transform2();
                }
                catch (Exception ex)
                {
                    tbResponse.Text = ex.ToString();
                    lblError.Visible = true;
                }
            }
        }

        private Boolean checkXmls(){
            lblError.Visible = false;
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(tbRequest.Text);
            }
            catch (Exception ex)
            {
                tbResponse.Text = ex.ToString();
                lblError.Visible= true;
                return false;
            }

            try
            {
                doc.Load("mock_data.xml");
            }
            catch (Exception ex)
            {
                tbResponse.Text = ex.ToString();
                lblError.Visible = true;
                return false;
            }
            return true;
        }

        //create XSLT from request
        private void Transform1() 
        {
            XslCompiledTransform trans = new XslCompiledTransform();
//            trans.Load(new XmlTextReader(new StringReader(tbTrans.Text)));
            trans.Load("trans.xsl");

            using (StringWriter sw = new StringWriter())
            using (XmlWriter xwo = XmlWriter.Create(sw, trans.OutputSettings))
            {
                trans.Transform(new XmlTextReader(new StringReader(tbRequest.Text)), xwo);
                transText2 = sw.ToString();
            }

        }

        //apply XSLT-transformation created from request to mock_data.xml
        private void Transform2() {
            XslCompiledTransform trans = new XslCompiledTransform();
            trans.Load(new XmlTextReader(new StringReader(transText2)));
//            trans.Load("trans2.xsl");

            System.Xml.XmlWriterSettings xmlWriterSettings = new System.Xml.XmlWriterSettings();
            xmlWriterSettings.Encoding = enc;
            xmlWriterSettings.Indent = true;
            xmlWriterSettings.IndentChars = "        ";
            xmlWriterSettings.NewLineChars = "\r\n";
            xmlWriterSettings.NewLineHandling = System.Xml.NewLineHandling.Replace;
            xmlWriterSettings.ConformanceLevel = System.Xml.ConformanceLevel.Document;

            using (StringWriter sw = new StringWriter())
            using (XmlWriter xwo = XmlWriter.Create(sw, xmlWriterSettings))
            {
                trans.Transform("mock_data.xml", xwo);
                tbResponse.Text = sw.ToString();
            }

        }

        private void btClear_Click(object sender, EventArgs e)
        {
            tbRequest.Clear();
        }

        private void tbRequest_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        private void tbRequest_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files.Length == 1) {
                string file = files[0];
                tbRequest.Text = File.ReadAllText(file, enc);
            }
        }
    }
}
