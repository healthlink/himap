﻿// Copyright (c) HealthLink 2015 All Right Reserved
// Author Chandan Datta (chandan.datta@healthlink.net)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace himap
{
    static class Program
    {
        /// <summary>
        /// @CHANDAN; TODO
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
