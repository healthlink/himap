## About
**Project HiMap** is a Microsoft .net C# based reference implementation of the Healthlink forms schema independent  concept resolution engine. 


----------


## Solution Design

![CRE solution design](/docs/solution_design.jpg?raw=true "CRE")


The concept resolution engine (CRE) maps the EMR’s own internal dataconcepts to the standardized Aduro concepts. The CRE needs to ensure the concept resolution is correct in a Healthlink forms schema independent way. It also needs to ensure that the concept modifier 
attributes are implemented for complex types in an arbitrary schema. Details are in the documentation - '2015-06_30_Healthlink forms schema independent concept resolution.pdf'

## Usage
* Launch the himap.exe file (himap\bin\Release) - make sure the trans.xml and mock_data.xml are in the same folder as the exe

* Drag-n-drop any of the sample test SOAP XML request files in the soap_xml_samples folder and see the response

![HiMap Launch](/docs/himap-dragndrop.jpg?raw=true "CRE")

* View the response 

![HiMap Response](/docs/himap-response.jpg?raw=true "CRE")

## System requirements
* Windows 7 x64 or above
* Windows 2008 R2 x64 or above
* Microsoft Visual Studio 2013

## Setup

HiMap is a Winforms application implemented using C# and doesn't have any specific .net SDK requirement. It has been tested with VS 2013 with the .NET Framework 4.5 that ships with it.

![Visual Studio Project](/docs/setup.jpg?raw=true "Visual Studio Project")